<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function contact_submissions_user_default_permissions() {
  $permissions = array();

  // Exported permission: create contact content
  $permissions['create contact content'] = array(
    'name' => 'create contact content',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: edit any contact content
  $permissions['edit any contact content'] = array(
    'name' => 'edit any contact content',
    'roles' => array(),
  );

  // Exported permission: edit field_contact_risposta
  $permissions['edit field_contact_risposta'] = array(
    'name' => 'edit field_contact_risposta',
    'roles' => array(
      '0' => 'Redattore Ras',
    ),
  );

  // Exported permission: view field_contact_risposta
  $permissions['view field_contact_risposta'] = array(
    'name' => 'view field_contact_risposta',
    'roles' => array(
      '0' => 'Redattore Ras',
    ),
  );

  return $permissions;
}
