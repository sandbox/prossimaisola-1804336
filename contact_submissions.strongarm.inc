<?php

/**
 * Implementation of hook_strongarm().
 */
function contact_submissions_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_contact';
  $strongarm->value = '1';

  $export['ant_contact'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_contact';
  $strongarm->value = array(
    'title' => '-5',
    'body_field' => '2',
    'revision_information' => '7',
    'author' => '6',
    'options' => '8',
    'comment_settings' => '9',
    'menu' => '-4',
    'book' => '4',
    'path' => '10',
    'print' => '11',
    'workflow' => '5',
  );

  $export['content_extra_weights_contact'] = $strongarm;
  return $export;
}
