<?php

/**
 * Implementation of hook_conditional_fields_default_fields().
 */
function contact_submissions_conditional_fields_default_fields() {
$items = array();
$items[] = array (
  'control_field_name' => 'field_contact_tematica',
  'field_name' => 'field_contact_altro',
  'type' => 'contact',
  'trigger_values' => 
  array (
    'Altro' => 'Altro',
  ),
);
return $items;
}
