<?php

/**
 * Implementation of hook_workflow_defaults().
 */
function contact_submissions_workflow_defaults() {
 $defaults = array();
  $defaults[] = array(
    'name' => 'Contact submission state',
    'machine_name' => 'contact_submission_state',
    'tab_roles' => array(
      '0' => 'admin',
      '1' => 'Redattore Ras',
    ),
    'options' => array(
      'comment_log_node' => 1,
      'comment_log_tab' => 1,
    ),
    'states' => array(
      '8' => array(
        'state' => '(creation)',
        'weight' => '-50',
        'sysid' => '1',
        'status' => '1',
        'module' => 'contact_submissions',
        'ref' => '8',
        'access' => array(),
      ),
      '9' => array(
        'state' => 'Sottomessa',
        'weight' => '0',
        'sysid' => '0',
        'status' => '1',
        'module' => 'contact_submissions',
        'ref' => '9',
        'access' => array(
          '0' => array(
            'role' => 'author',
            'rid' => '-1',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '1' => array(
            'role' => 'Unica',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '2' => array(
            'role' => 'Unibo',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '3' => array(
            'role' => 'Supervisore',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '4' => array(
            'role' => 'Sitcrgreen',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '5' => array(
            'role' => 'Operatore',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '6' => array(
            'role' => 'manager',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '7' => array(
            'role' => 'grafici',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '8' => array(
            'role' => 'Funzionario Regionale',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '9' => array(
            'role' => 'Data entry Associazione',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '10' => array(
            'role' => 'Community manager',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '11' => array(
            'role' => 'Beta tester',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '12' => array(
            'role' => 'Abbila',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '13' => array(
            'role' => 'authenticated user',
            'rid' => '2',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '14' => array(
            'role' => 'anonymous user',
            'rid' => '1',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '15' => array(
            'role' => 'Redattore Ras',
            'grant_view' => '1',
            'grant_update' => '1',
            'grant_delete' => '0',
          ),
          '16' => array(
            'role' => 'admin',
            'grant_view' => '1',
            'grant_update' => '1',
            'grant_delete' => '0',
          ),
        ),
      ),
      '10' => array(
        'state' => 'In risposta',
        'weight' => '1',
        'sysid' => '0',
        'status' => '1',
        'module' => 'contact_submissions',
        'ref' => '10',
        'access' => array(
          '0' => array(
            'role' => 'author',
            'rid' => '-1',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '1' => array(
            'role' => 'Unica',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '2' => array(
            'role' => 'Unibo',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '3' => array(
            'role' => 'Supervisore',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '4' => array(
            'role' => 'Sitcrgreen',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '5' => array(
            'role' => 'Operatore',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '6' => array(
            'role' => 'manager',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '7' => array(
            'role' => 'grafici',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '8' => array(
            'role' => 'Funzionario Regionale',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '9' => array(
            'role' => 'Data entry Associazione',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '10' => array(
            'role' => 'Community manager',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '11' => array(
            'role' => 'Beta tester',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '12' => array(
            'role' => 'Abbila',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '13' => array(
            'role' => 'authenticated user',
            'rid' => '2',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '14' => array(
            'role' => 'anonymous user',
            'rid' => '1',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '15' => array(
            'role' => 'Redattore Ras',
            'grant_view' => '1',
            'grant_update' => '1',
            'grant_delete' => '0',
          ),
          '16' => array(
            'role' => 'admin',
            'grant_view' => '1',
            'grant_update' => '1',
            'grant_delete' => '0',
          ),
        ),
      ),
      '11' => array(
        'state' => 'Email inviata',
        'weight' => '2',
        'sysid' => '0',
        'status' => '1',
        'module' => 'contact_submissions',
        'ref' => '11',
        'access' => array(
          '0' => array(
            'role' => 'author',
            'rid' => '-1',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '1' => array(
            'role' => 'Unica',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '2' => array(
            'role' => 'Unibo',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '3' => array(
            'role' => 'Supervisore',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '4' => array(
            'role' => 'Sitcrgreen',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '5' => array(
            'role' => 'Operatore',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '6' => array(
            'role' => 'manager',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '7' => array(
            'role' => 'grafici',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '8' => array(
            'role' => 'Funzionario Regionale',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '9' => array(
            'role' => 'Data entry Associazione',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '10' => array(
            'role' => 'Community manager',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '11' => array(
            'role' => 'Beta tester',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '12' => array(
            'role' => 'Abbila',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '13' => array(
            'role' => 'authenticated user',
            'rid' => '2',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '14' => array(
            'role' => 'anonymous user',
            'rid' => '1',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '15' => array(
            'role' => 'Redattore Ras',
            'grant_view' => '1',
            'grant_update' => '1',
            'grant_delete' => '0',
          ),
          '16' => array(
            'role' => 'admin',
            'grant_view' => '1',
            'grant_update' => '1',
            'grant_delete' => '0',
          ),
        ),
      ),
    ),
    'roles' => array(
      'author' => array(
        'name' => 'author',
        'transitions' => array(
          '0' => array(
            'from' => '8',
            'to' => '9',
          ),
        ),
      ),
      'anonymous user' => array(
        'name' => 'anonymous user',
        'transitions' => array(
          '0' => array(
            'from' => '8',
            'to' => '9',
          ),
        ),
      ),
      'admin' => array(
        'name' => 'admin',
        'transitions' => array(
          '0' => array(
            'from' => '8',
            'to' => '9',
          ),
        ),
      ),
      'Redattore Ras' => array(
        'name' => 'Redattore Ras',
        'transitions' => array(
          '0' => array(
            'from' => '9',
            'to' => '10',
          ),
          '1' => array(
            'from' => '10',
            'to' => '11',
          ),
        ),
      ),
    ),
    'types' => array(
      'contact' => 'contact',
    ),
  );
  return $defaults;

}
