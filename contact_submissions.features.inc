<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function contact_submissions_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function contact_submissions_node_info() {
  $items = array(
    'contact' => array(
      'name' => t('Contatto'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titolo'),
      'has_body' => '1',
      'body_label' => t('Messaggio'),
      'min_word_count' => '1',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_rules_defaults().
 */
function contact_submissions_rules_defaults() {
  return array(
    'rules' => array(
      'contact_submissions_1' => array(
        '#type' => 'rule',
        '#set' => 'event_node_insert',
        '#label' => 'Invia risposta automatica del form contatto',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'Form di contatto',
          'contact_submissions' => 'contact_submissions',
        ),
        '#status' => 'default',
        '#conditions' => array(
          '0' => array(
            '#weight' => 0,
            '#info' => array(
              'label' => 'Created content is Contact',
              'arguments' => array(
                'node' => array(
                  'type' => 'node',
                  'label' => 'Contenuto',
                ),
              ),
              'module' => 'Node',
            ),
            '#name' => 'rules_condition_content_is_type',
            '#settings' => array(
              'type' => array(
                'contact' => 'contact',
              ),
              '#argument map' => array(
                'node' => 'node',
              ),
            ),
            '#type' => 'condition',
          ),
        ),
        '#actions' => array(
          '0' => array(
            '#type' => 'action',
            '#settings' => array(
              'to' => '[node:field_contact_mail-raw]',
              'cc' => '',
              'bcc' => '',
              'from' => '',
              'subject' => 'Grazie per aver compilato il nostro form di contatto',
              'message_html' => 'Ciao [node:field_contact_name-raw],
grazie per aver compilato il nostro form di contatto, ti risponderemo al più presto.',
              'message_plaintext' => '',
              'attachments' => '',
              '#eval input' => array(
                'token_rules_input_evaluator' => array(
                  'to' => array(
                    '0' => 'node',
                  ),
                  'message_html' => array(
                    '0' => 'node',
                  ),
                ),
              ),
            ),
            '#name' => 'mimemail_rules_action_mail',
            '#info' => array(
              'label' => 'Send an HTML mail to an arbitrary mail address',
              'module' => 'Mime Mail',
              'eval input' => array(
                '0' => 'from',
                '1' => 'to',
                '2' => 'cc',
                '3' => 'bcc',
                '4' => 'subject',
                '5' => 'message_html',
                '6' => 'message_plaintext',
                '7' => 'attachments',
              ),
            ),
            '#weight' => 0,
          ),
          '1' => array(
            '#info' => array(
              'module' => 'Workflow',
              'arguments' => array(
                'node' => array(
                  'label' => 'Contenuto',
                  'type' => 'node',
                ),
              ),
              'label' => 'Change workflow state of contenuto to new state',
              'base' => 'rules_core_action_execute',
              'action_name' => 'workflow_select_given_state_action',
              'configurable' => TRUE,
            ),
            '#name' => 'rules_core_workflow_select_given_state_action',
            '#settings' => array(
              'target_sid' => '10',
              'state_name' => 'In risposta',
              'force' => 1,
              'workflow_comment' => 'Action set %title to %state.',
              '#argument map' => array(
                'node' => 'node',
              ),
            ),
            '#type' => 'action',
            '#weight' => 0,
          ),
          '2' => array(
            '#type' => 'action',
            '#settings' => array(
              'code' => 'drupal_set_message(\'Grazie per aver compilato il form di contatto.\');',
              'vars' => array(),
            ),
            '#name' => 'rules_action_custom_php',
            '#info' => array(
              'label' => 'Execute custom PHP code',
              'module' => 'PHP',
              'eval input' => array(
                '0' => 'code',
              ),
            ),
            '#weight' => 0,
          ),
          '3' => array(
            '#weight' => 0,
            '#info' => array(
              'label' => 'Page redirect',
              'module' => 'System',
              'eval input' => array(
                '0' => 'path',
                '1' => 'query',
                '2' => 'fragment',
              ),
            ),
            '#name' => 'rules_action_drupal_goto',
            '#settings' => array(
              'path' => '/',
              'query' => '',
              'fragment' => '',
              'force' => 1,
              'immediate' => 0,
            ),
            '#type' => 'action',
          ),
        ),
        '#version' => 6003,
      ),
      'contact_submissions_2' => array(
        '#type' => 'rule',
        '#set' => 'event_workflow_state_changed',
        '#label' => 'Invio risposta form di contatto',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'Form di contatto',
          'contact_submissions' => 'contact_submissions',
        ),
        '#status' => 'default',
        '#conditions' => array(
          '0' => array(
            '#weight' => 0,
            '#info' => array(
              'label' => 'Updated content is Contatto',
              'arguments' => array(
                'node' => array(
                  'type' => 'node',
                  'label' => 'Contenuto',
                ),
              ),
              'module' => 'Node',
            ),
            '#name' => 'rules_condition_content_is_type',
            '#settings' => array(
              'type' => array(
                'contact' => 'contact',
              ),
              '#argument map' => array(
                'node' => 'node',
              ),
            ),
            '#type' => 'condition',
          ),
          '1' => array(
            '#type' => 'condition',
            '#settings' => array(
              'from_state' => array(
                '10' => '10',
              ),
              'to_state' => array(
                '11' => '11',
              ),
              '#argument map' => array(
                'old_state' => 'old_state',
                'new_state' => 'new_state',
              ),
            ),
            '#name' => 'workflow_check_transition',
            '#info' => array(
              'label' => 'Check workflow transition from In risposta to Email inviata',
              'arguments' => array(
                'old_state' => array(
                  'type' => 'workflow_state',
                  'label' => 'Old workflow state',
                ),
                'new_state' => array(
                  'type' => 'workflow_state',
                  'label' => 'New workflow state',
                ),
              ),
              'module' => 'Workflow',
            ),
            '#weight' => 0,
          ),
        ),
        '#actions' => array(
          '2' => array(
            '#info' => array(
              'label' => 'Send an HTML mail to an arbitrary mail address',
              'module' => 'Mime Mail',
              'eval input' => array(
                '0' => 'from',
                '1' => 'to',
                '2' => 'cc',
                '3' => 'bcc',
                '4' => 'subject',
                '5' => 'message_html',
                '6' => 'message_plaintext',
                '7' => 'attachments',
              ),
            ),
            '#name' => 'mimemail_rules_action_mail',
            '#settings' => array(
              'to' => '[node:field_contact_mail-raw]',
              'cc' => '',
              'bcc' => '',
              'from' => '',
              'subject' => 'Risposta form di contatto',
              'message_html' => 'L\'operatore ha risposto:

[node:field_contact_risposta-raw]',
              'message_plaintext' => '',
              'attachments' => '',
              '#eval input' => array(
                'token_rules_input_evaluator' => array(
                  'to' => array(
                    '0' => 'node',
                  ),
                  'message_html' => array(
                    '0' => 'node',
                  ),
                ),
              ),
            ),
            '#type' => 'action',
            '#weight' => 0,
          ),
          '1' => array(
            '#type' => 'action',
            '#settings' => array(
              'code' => 'drupal_set_message("Il sistemà provvederà all\'invio dell\'email");',
              'vars' => array(),
            ),
            '#name' => 'rules_action_custom_php',
            '#info' => array(
              'label' => 'Execute custom PHP code',
              'module' => 'PHP',
              'eval input' => array(
                '0' => 'code',
              ),
            ),
            '#weight' => 1,
          ),
        ),
        '#version' => 6003,
      ),
    ),
  );
}

/**
 * Implementation of hook_views_api().
 */
function contact_submissions_views_api() {
  return array(
    'api' => '2',
  );
}
